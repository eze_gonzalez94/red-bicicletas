var Bicicleta = require('../../../models/bicicleta');
var mongoose = require('mongoose');


describe('Testing Bicicletas', function (){
    beforeEach(function(done) {
        //jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database :)');
            done();
        });
        
    });
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect();
            done();
        });
    });

    describe('Bicicleta.createInstance', ()=> {
        it('crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {    
            var a = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana'});
            Bicicleta.add(a, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(a.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con el code 1', (done) => {
             Bicicleta.allBicis(function(err, bicis) {
                 expect(bicis.length).toBe(0);

                 var bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                 Bicicleta.add(bici, function (err, newBici) {
                    if (err) console.log(err);
                    
                    var bici2 = new Bicicleta({code:2, color: 'roja', modelo: 'urbana'});
                    Bicicleta.add(bici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici) {
                            expect(targetBici.code).toBe(bici.code);
                            expect(targetBici.color).toBe(bici.color);
                            expect(targetBici.modelo).toBe(bici.modelo);

                            done();
                        });
                    });
                     
                 });
             });
        });
    });

});


/* // ejecuta antes de cada prueba
beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        // precondicion
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6180584,-58.4702042]);
        Bicicleta.add(a);

        // postcondicion
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('Debe devolver la bici con ID 1', () => {
        // precondicion
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6180584,-58.4702042]);
        var b = new Bicicleta(2, 'azul', 'playera', [-34.613292,-58.4582065]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(a.id);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);


        // postcondicion
        expect(Bicicleta.allBicis.length).toBe(2);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
}); */