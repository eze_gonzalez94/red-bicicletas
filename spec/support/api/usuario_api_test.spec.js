var Usuario = require('../../../models/usuario');
var request = require('request');
var server = require('../../../bin/www');
var mongoose = require('mongoose');

var base_url = "http://localhost:3000/api/usuarios";

describe('Usuarios API', function() {
    beforeEach(function(done) {

        var mongoDB = 'mongodb://localhost/testdbAPI';
        mongoose.connect(mongoDB, { userNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Usuario.deleteMany({}, function(err, succes){
            if (err) console.log(err)
            mongoose.disconnect();
            done();
        });
    });

    beforeAll((done) => { 
        mongoose.connection.close(done) 
    });

    describe('GET USUARIOS', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.usuarios.length).toBe(0);
                done();
            });
        });
    });

});

