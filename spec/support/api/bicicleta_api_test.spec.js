var Bicicleta = require('../../../models/bicicleta');
var request = require('request');
var server = require('../../../bin/www');
var mongoose = require('mongoose');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', function() {
    beforeEach(function(done) {

        var mongoDB = 'mongodb://localhost/testdbAPI';
        mongoose.connect(mongoDB, { userNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, succes){
            if (err) console.log(err)
            console.log('Desconectando de la base');
            mongoose.disconnect();
            done();
        });
    });

    afterAll(function() {
        mongoose.disconnect();
        console.log('Terminando pruebas');
    });

    beforeAll((done) => { 
        mongoose.connection.close(done) 
    });

    describe('GET BICICLETAS', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicis.length).toBe(0);
                done();
            });
        });
    });
    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var a = '{ "code": 10, "color": "amarillo", "modelo": "urbana", "lat": -34.6180584, "lng": -58.4702042 }';
    
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: a
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.code).toBe(10);
                expect(bici.color).toBe('amarillo');
                expect(bici.modelo).toBe('urbana');
                expect(bici.ubicacion[0]).toBe(-34.6180584);
                expect(bici.ubicacion[1]).toBe(-58.4702042);
                done();
            });
        });
    });

});

// jasmine spec/support/api/bicicleta_api_test.spec.js

/* // NOTA: CUANDO SE HACE EL require, SE EJECUTA TODO LO QUE ESTA DENTRO
var Bicicleta = require('../../../models/bicicleta');
var request = require('request');

// para levantar el server
var server = require('../../../bin/www');

beforeEach(() => {
    //console.log('testeando…');
});

describe('Bicicleta API', () => {

    describe('GET BICICLETAS', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6180584, -58.4702042]);

            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200);
            });

        });
    });
    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var a = '{ "id": 10, "color": "amarillo", "modelo": "urbana", "lat": -34.6180584, "lng": -58.4702042 }';
    
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: a
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('amarillo');
                // se agrega el metodo done para hacerlo sincronico
                done();
            });
        });
    });
});

//HACER DELETE Y UPDATE
 */