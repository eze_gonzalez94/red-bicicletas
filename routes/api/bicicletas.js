var express = require('express');
var router = express.Router();
var bicicletaControllerApi = require('../../controllers/api/bicicletaControllerAPI');

//metodos de la api
router.get('/', bicicletaControllerApi.bicicleta_list);
router.post('/create', bicicletaControllerApi.bicicleta_create);
router.delete('/delete', bicicletaControllerApi.bicicleta_delete);
router.put('/update', bicicletaControllerApi.bicicleta_update);

module.exports = router;