var express = require('express');
var router = express.Router();
var reservaControllerApi = require('../../controllers/api/reservaControllerAPI');

//metodos de la api
router.get('/', reservaControllerApi.reservas_list);

module.exports = router;