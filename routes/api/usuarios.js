var express = require('express');
var router = express.Router();
var usuarioControllerApi = require('../../controllers/api/usuarioControllerAPI');

//metodos de la api
router.get('/', usuarioControllerApi.usuarios_list);
router.post('/create', usuarioControllerApi.usuarios_create);


module.exports = router;