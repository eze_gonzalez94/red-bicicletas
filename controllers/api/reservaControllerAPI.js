var mongoose = require('mongoose');
var Reserva = require('../../models/reserva');

exports.reservas_list = function (req,res) {
    Reserva.allReservas(function(err, reservas) {
        res.status(200).json({
            reservas: reservas
        });   
    });
}
