var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req,res) {
    Bicicleta.allBicis(function(err, bicis) {
        res.status(200).json({
            bicis: bicis
        });   
    });
}

exports.bicicleta_create = function (req, res) {
    var b = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    b.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(b, function(err, newBici) {
        res.status(200).json({
            bicicleta: b
        });
    });
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.code, function(err, bici) {
        res.status(204).send();
    });
}

exports.bicicleta_update = function (req,res) {
    var bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
}