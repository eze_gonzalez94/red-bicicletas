var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.removeById = function (id) {
    //Bicicleta.findById(id);
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == id) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

Bicicleta.findById = function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici) 
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    
}

/* var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6180584,-58.4702042]);
var b = new Bicicleta(2, 'azul', 'playera', [-34.613292,-58.4582065]);

Bicicleta.add(a);
Bicicleta.add(b); */

module.exports = Bicicleta;