var mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
var Schema = mongoose.Schema;

var bicicletaShema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion : {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
});

bicicletaShema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this ({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

bicicletaShema.methods.toString = function () {
    return 'code: ' + this.code + " | color: " + this.color;
}

bicicletaShema.statics.allBicis = function (callback) {
    return this.find({}, callback);
}

bicicletaShema.statics.add = function (aBici, callback) {
    this.create(aBici, callback); 
}

bicicletaShema.statics.findByCode = function (aCode, callback) {
    return this.findOne({code: aCode}, callback);
}

bicicletaShema.statics.removeByCode = function (aCode, callback) {
    return this.deleteOne({code: aCode}, callback);
}

module.exports = mongoose.model('Bicicleta', bicicletaShema);